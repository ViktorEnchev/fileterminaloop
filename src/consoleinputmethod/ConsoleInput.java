package consoleinputmethod;

import java.util.Scanner;

public class ConsoleInput {

    private static Scanner in = new Scanner(System.in);

    private static boolean rightNumberInput(int number, int size) {
        if (number < 1 || number > size) {
            return false;
        }
        return true;
    }

    public static int typeANumber(int size) {
        int number;
        while (true) {
            try {
                number = Integer.parseInt(in.nextLine());
                if (rightNumberInput(number, size)) {
                    break;
                } else {
                    System.out.println("Wrong input!");
                }
            } catch (Exception e) {
                System.out.println("That is not a number!");
            }
        }
        return number;
    }

    public static String typeText() {
        String name = in.nextLine();
        return name;
    }

}
