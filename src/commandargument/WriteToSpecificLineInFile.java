package commandargument;

import java.io.FileNotFoundException;
import java.util.List;

import com.sun.media.sound.InvalidFormatException;

import command.Command;
import filesystem.File;
import filesystem.FileSystem;

public class WriteToSpecificLineInFile implements Command {

    private FileSystem fileSystem;
    private String fileName;
    private String text;
    private int line;

    public void setText(String text) {
        this.text = text;
    }

    public void setLine(int line) {
        this.line = line;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public WriteToSpecificLineInFile(FileSystem fileSystem) {
        this.fileSystem = fileSystem;
    }

    public String execute(List<String> currentPath) throws FileNotFoundException, InvalidFormatException {
        fileSystem.changeToCurrentDirectory(currentPath);
        if (!fileSystem.contains(fileName)) {
            fileSystem.goBackToRoot();
            throw new FileNotFoundException(fileName + " doesn't exist");
        } else {
            if (fileSystem.checkIfFileIsRightType(fileName, "file")) {
                File file = fileSystem.getFile(fileName);
                file.overWriteTextToSpecificLineInFile(line, text);
            } else {
                fileSystem.goBackToRoot();
                throw new InvalidFormatException(fileName + "is not a directory");
            }
        }
        fileSystem.goBackToRoot();
        return "";
    }

    @Override
    public void setCommandLineArguments(List<String> commandLine) {
        fileName = commandLine.get(2);
        line = Integer.parseInt(commandLine.get(3));
        getTextFromCommandLine(commandLine);
    }

    private void getTextFromCommandLine(List<String> commandLine) {
        text = "";
        for (int i = 4; i < commandLine.size(); i++) {
            text += commandLine.get(i) + " ";
        }
    }
}
