package commandargument;

import java.util.List;

import command.Command;
import filesystem.FileSystem;

public class ChangeDirectoryToSelf implements Command {

    @SuppressWarnings("unused")
    private FileSystem fileSystem;

    public ChangeDirectoryToSelf(FileSystem fileSystem) {
        this.fileSystem = fileSystem;
    }

    @Override
    public String execute(List<String> currentPath) {
        return "";
    }

    @Override
    public void setCommandLineArguments(List<String> commandLine) {
        // TODO Auto-generated method stub

    }

}
