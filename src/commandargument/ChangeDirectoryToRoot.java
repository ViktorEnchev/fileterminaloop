package commandargument;

import java.util.List;

import command.Command;
import filesystem.FileSystem;

public class ChangeDirectoryToRoot implements Command {

    private FileSystem fileSystem;

    public ChangeDirectoryToRoot(FileSystem fileSystem) {
        this.fileSystem = fileSystem;
    }

    @Override
    public String execute(List<String> currentPath) {
        fileSystem.changeDirctoryToRoot(currentPath);
        return "";
    }

    @Override
    public void setCommandLineArguments(List<String> commandLine) {
        // TODO Auto-generated method stub

    }

}
