package commandargument;

import java.util.List;

import command.Command;
import filesystem.FileSystem;

public class ChangeDirectoryToParent implements Command {

    private FileSystem fileSystem;

    public ChangeDirectoryToParent(FileSystem fileSystem) {
        this.fileSystem = fileSystem;
    }

    @Override
    public String execute(List<String> currentPath) {
        fileSystem.changeDirctoryToParent(currentPath);
        return "";
    }

    @Override
    public void setCommandLineArguments(List<String> commandLine) {
        // TODO Auto-generated method stub

    }

}
