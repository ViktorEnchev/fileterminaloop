package main;

import java.nio.file.FileAlreadyExistsException;

import filesystem.FileSystem;
import terminal.Terminal;

public class Main {

    public static void main(String[] args) throws FileAlreadyExistsException {
        FileSystem fileSystem = new FileSystem();
        Terminal terminal = new Terminal(fileSystem, fileSystem.getPath());
        terminal.run();
    }
}