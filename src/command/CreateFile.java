package command;

import java.nio.file.FileAlreadyExistsException;
import java.util.HashMap;
import java.util.List;

import filesystem.Directory;
import filesystem.FileSystem;

public class CreateFile extends PrimeCommand {

    private String fileName;

    public CreateFile(FileSystem fileSystem) {
        super(fileSystem);
    }

    @Override
    protected void setMap() {
        minorOperations = new HashMap<>();
    }

    @Override
    public String execute(List<String> currentPath) throws FileAlreadyExistsException {
        fileSystem.changeToCurrentDirectory(currentPath);
        if (fileSystem.contains(fileName)) {
            fileSystem.goBackToRoot();
            throw new FileAlreadyExistsException(fileName + " already exists");
        }
        Directory currentDirectory = fileSystem.getRoot();
        currentDirectory.addDataToDirectory(fileName, "file");
        fileSystem.goBackToRoot();
        return "";
    }

    @Override
    public Command getCommandArgument(List<String> commandLine) {
        if (commandLine.size() == 2) {
            setCommandLineArguments(commandLine);
            return this;
        } else if (commandLine.size() == 1) {
            return null;
        }
        Command commandArgument = minorOperations.get(commandLine.get(1));
        if (commandArgument == null) {
            throw new IllegalArgumentException("invalid option: " + commandLine.get(1));
        }
        return commandArgument;
    }

    @Override
    public void setCommandLineArguments(List<String> commandLine) {
        fileName = commandLine.get(1);
    }
}
