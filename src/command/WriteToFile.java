package command;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;

import com.sun.media.sound.InvalidFormatException;

import commandargument.WriteToSpecificLineInFile;
import filesystem.File;
import filesystem.FileSystem;

public class WriteToFile extends PrimeCommand {

    private String fileName;
    private String text;
    private int line;

    public WriteToFile(FileSystem fileSystem) {
        super(fileSystem);
    }

    @Override
    protected void setMap() {
        minorOperations = new HashMap<>();
        minorOperations.put("--overwrite", new WriteToSpecificLineInFile(fileSystem));
    }

    @Override
    public String execute(List<String> currentPath) throws FileNotFoundException, InvalidFormatException {
        fileSystem.changeToCurrentDirectory(currentPath);
        if (!fileSystem.contains(fileName)) {
            fileSystem.goBackToRoot();
            throw new FileNotFoundException(fileName + " doesn't exist");
        } else {
            if (fileSystem.checkIfFileIsRightType(fileName, "file")) {
                File file = fileSystem.getFile(fileName);
                file.appendTextToSpecificLineInFile(line, text);
            } else {
                fileSystem.goBackToRoot();
                throw new InvalidFormatException(fileName + "is not a directory");
            }
        }
        fileSystem.goBackToRoot();
        return "";
    }

    @Override
    public Command getCommandArgument(List<String> commandLine) {
        if (commandLine.size() < 4) {
            return null;
        }
        if (Character.isDigit(commandLine.get(2).charAt(0))) {
            setCommandLineArguments(commandLine);
            return this;
        }
        if (!Character.isDigit(commandLine.get(3).charAt(0))) {
            return null;
        }
        Command commandArgument = minorOperations.get(commandLine.get(1));
        if (commandArgument == null) {
            throw new IllegalArgumentException("invalid option: " + commandLine.get(1));
        } else {
            commandArgument.setCommandLineArguments(commandLine);
        }
        return commandArgument;
    }

    @Override
    public void setCommandLineArguments(List<String> commandLine) {
        fileName = commandLine.get(1);
        getTextFromCommandLine(commandLine);
        line = Integer.parseInt(commandLine.get(2));
    }

    private void getTextFromCommandLine(List<String> commandLine) {
        text = "";
        for (int i = 3; i < commandLine.size(); i++) {
            text += commandLine.get(i) + " ";
        }
    }
}
