package command;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;

import com.sun.media.sound.InvalidFormatException;

import filesystem.File;
import filesystem.FileSystem;

public class ConcatenateFile extends PrimeCommand {

    String fileName;

    public ConcatenateFile(FileSystem fileSystem) {
        super(fileSystem);
    }

    @Override
    protected void setMap() {
        minorOperations = new HashMap<>();
    }

    @Override
    public String execute(List<String> currentPath) throws FileNotFoundException, InvalidFormatException {
        fileSystem.changeToCurrentDirectory(currentPath);
        String result = "";
        if (!fileSystem.contains(fileName)) {
            fileSystem.goBackToRoot();
            throw new FileNotFoundException(fileName + " doesn't exist");
        } else {
            if (fileSystem.checkIfFileIsRightType(fileName, "file")) {
                File file = fileSystem.getFile(fileName);
                result = file.getContent();
                System.out.println(result);
            } else {
                fileSystem.goBackToRoot();
                throw new InvalidFormatException(fileName + "is not a directory");
            }
        }
        fileSystem.goBackToRoot();
        return result;
    }

    @Override
    public Command getCommandArgument(List<String> commandLine) {
        if (commandLine.size() == 2) {
            setCommandLineArguments(commandLine);
            return this;
        } else if (commandLine.size() == 1) {
            return null;
        }
        Command commandArgument = minorOperations.get(commandLine.get(1));
        if (commandArgument == null) {
            throw new IllegalArgumentException("invalid option: " + commandLine.get(1));
        }
        return commandArgument;
    }

    @Override
    public void setCommandLineArguments(List<String> commandLine) {
        fileName = commandLine.get(1);
    }
}
