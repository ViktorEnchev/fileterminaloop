package command;

import java.util.HashMap;
import java.util.List;

import filesystem.Directory;
import filesystem.FileSystem;

public class ListDirectory extends PrimeCommand {

    public ListDirectory(FileSystem fileSystem) {
        super(fileSystem);
    }

    @Override
    protected void setMap() {
        minorOperations = new HashMap<>();
    }

    @Override
    public String execute(List<String> currentPath) {
        fileSystem.changeToCurrentDirectory(currentPath);
        Directory currentDirectory = fileSystem.getRoot();
        String result = currentDirectory.listFilesAndFolders();
        System.out.println(result);
        fileSystem.goBackToRoot();
        return result;
    }

    @Override
    public Command getCommandArgument(List<String> commandLine) {
        if (commandLine.size() == 1) {
            return this;
        }
        Command commandArgument = minorOperations.get(commandLine.get(1));
        if (commandArgument == null) {
            throw new IllegalArgumentException("invalid option: " + commandLine.get(1));
        }
        return commandArgument;
    }

    @Override
    public void setCommandLineArguments(List<String> commandLine) {
        // TODO Auto-generated method stub

    }
}
