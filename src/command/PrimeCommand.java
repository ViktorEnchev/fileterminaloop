package command;

import java.util.List;
import java.util.Map;

import filesystem.FileSystem;

public abstract class PrimeCommand implements Command {

    protected FileSystem fileSystem;
    protected Map<String, Command> minorOperations;

    public PrimeCommand(FileSystem fileSystem) {
        this.fileSystem = fileSystem;
        setMap();
    }

    protected abstract void setMap();

    public abstract Command getCommandArgument(List<String> commandLine);
}
