package command;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;

import com.sun.media.sound.InvalidFormatException;

import commandargument.ChangeDirectoryToParent;
import commandargument.ChangeDirectoryToRoot;
import commandargument.ChangeDirectoryToSelf;
import filesystem.FileSystem;

public class ChangeDirectory extends PrimeCommand {

    private String directoryName;

    public ChangeDirectory(FileSystem fileSystem) {
        super(fileSystem);
    }

    @Override
    protected void setMap() {
        minorOperations = new HashMap<>();
        minorOperations.put("root", new ChangeDirectoryToRoot(fileSystem));
        minorOperations.put(".", new ChangeDirectoryToSelf(fileSystem));
        minorOperations.put("..", new ChangeDirectoryToParent(fileSystem));
    }

    @Override
    public String execute(List<String> currentPath) throws FileNotFoundException, InvalidFormatException {
        fileSystem.changeToCurrentDirectory(currentPath);
        if (!fileSystem.contains(directoryName)) {
            fileSystem.goBackToRoot();
            throw new FileNotFoundException(directoryName + " doesn't exist");
        } else {
            if (fileSystem.checkIfFileIsRightType(directoryName, "directory")) {
                currentPath.add(directoryName);
            } else {
                fileSystem.goBackToRoot();
                throw new InvalidFormatException(directoryName + " is not a directory");
            }
        }
        fileSystem.goBackToRoot();
        return "";
    }

    @Override
    public Command getCommandArgument(List<String> commandLine) {
        if (commandLine.size() == 1) {
            return minorOperations.get("root");
        }
        Command commandArgument = minorOperations.get(commandLine.get(1));
        if (commandArgument == null) {
            setCommandLineArguments(commandLine);
            commandArgument = this;
        }
        return commandArgument;
    }

    @Override
    public void setCommandLineArguments(List<String> commandLine) {
        directoryName = commandLine.get(1);
    }
}
