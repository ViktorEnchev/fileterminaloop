package command;

import java.io.FileNotFoundException;
import java.nio.file.FileAlreadyExistsException;
import java.util.List;

import com.sun.media.sound.InvalidFormatException;

public interface Command {

    public String execute(List<String> currentPath)
            throws FileAlreadyExistsException, FileNotFoundException, InvalidFormatException;

    public void setCommandLineArguments(List<String> commandLine);

}
