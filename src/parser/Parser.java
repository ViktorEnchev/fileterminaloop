package parser;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import command.ChangeDirectory;
import command.Command;
import command.ConcatenateFile;
import command.CreateFile;
import command.ListDirectory;
import command.MakeDirectory;
import command.PrimeCommand;
import command.WriteToFile;
import filesystem.FileSystem;

public class Parser {

    private FileSystem fileSystem;
    private Map<String, Command> commands;

    public Parser(FileSystem fileSystem) {
        this.fileSystem = fileSystem;
        setMap();
    }

    private void setMap() {
        commands = new HashMap<>();
        commands.put("ls", new ListDirectory(fileSystem));
        commands.put("mkdir", new MakeDirectory(fileSystem));
        commands.put("create_file", new CreateFile(fileSystem));
        commands.put("cd", new ChangeDirectory(fileSystem));
        commands.put("write_file", new WriteToFile(fileSystem));
        commands.put("cat", new ConcatenateFile(fileSystem));
    }

    public Command getOperation(String chosenCommand, List<String> commandLine) {
        PrimeCommand command = (PrimeCommand) commands.get(chosenCommand);
        if (command == null) {
            throw new IllegalArgumentException("command not found: " + chosenCommand);
        }
        Command actualCommand = command.getCommandArgument(commandLine);
        if (actualCommand == null) {
            throw new IllegalArgumentException(chosenCommand + ":missing operand");
        }
        return command;
    }
}
