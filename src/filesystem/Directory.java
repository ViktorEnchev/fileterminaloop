package filesystem;

import java.util.HashMap;
import java.util.Map;

public class Directory extends Data {

    private Map<String, Data> filesAndFolders;
    private int size; // line + characters

    public Directory(String name, Directory parent) {
        super(name, parent, "directory");
        filesAndFolders = new HashMap<>();
    }

    public boolean addDataToDirectory(String name, String type) {
        if (type.equals("directory")) {
            filesAndFolders.put(name, new Directory(name, this));
            return true;
        } else if (type.equals("file")) {
            filesAndFolders.put(name, new File(name, this));
            return true;
        }
        return false;
    }

    public String listFilesAndFolders() {
        StringBuilder result = new StringBuilder();
        for (String name : filesAndFolders.keySet()) {
            result.append(name + " ");
        }
        return result.toString();
    }

    protected boolean checkIfFileExistsInDirectory(String name) {
        return filesAndFolders.containsKey(name);
    }

    protected boolean checkIfFileIsRightType(String name, String type) {
        return filesAndFolders.get(name).getType().equals(type);
    }

    protected File getFile(String fileName) {
        return (File) filesAndFolders.get(fileName);
    }

    protected Data getDirectory(String name) {
        return filesAndFolders.get(name);
    }

    protected Data returnParent() {
        if (getParent() == null) {
            return this;
        }
        return getParent();
    }
}
