package filesystem;

public abstract class Data {

    private String name;
    private Directory parent;
    private String type;

    public Data(String name, Directory parent, String type) {
        this.name = name;
        this.parent = parent;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    protected Data getParent() {
        return parent;
    }

    public String getType() {
        return type;
    }
}
