package filesystem;

import java.util.ArrayList;
import java.util.List;

public class File extends Data {

    private List<String> content;
    private int size;

    public File(String name, Directory parent) {
        super(name, parent, "file");
        content = new ArrayList<>();
        content.add("");
    }

    public boolean appendTextToSpecificLineInFile(int line, String text) {
        if (content.size() < line) {
            for (int i = content.size(); i < line; i++) {
                content.add("");
            }
            content.set(line - 1, content.get(line - 1) + text);
            return true;
        } else {
            content.set(line - 1, content.get(line - 1) + text);
            return true;
        }
    }

    public boolean overWriteTextToSpecificLineInFile(int line, String text) {
        if (content.size() < line) {
            for (int i = content.size(); i < line; i++) {
                content.add("");
            }
            content.set(line - 1, text);
            return true;
        } else {
            content.set(line - 1, text);
            return true;
        }
    }

    public String getContent() {
        String result = "";
        for (String string : content) {
            result += string + "\n";
        }
        return result;
    }
}
