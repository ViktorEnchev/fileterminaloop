package filesystem;

import java.util.ArrayList;
import java.util.List;

public class FileSystem {

    private Directory root;

    public FileSystem() {
        root = new Directory("/", null);
        root.addDataToDirectory("home", "directory");
    }

    public boolean contains(String name) {
        return root.checkIfFileExistsInDirectory(name);
    }

    public boolean checkIfFileIsRightType(String name, String type) {
        return root.checkIfFileIsRightType(name, type);
    }

    public List<String> getPath() {
        List<String> path = new ArrayList<>();
        path.add("home");
        return path;
    }

    public void changeDirctoryToParent(List<String> currentPath) {
        if (currentPath.size() > 0) {
            currentPath.remove(currentPath.size() - 1);
        }
    }

    public void changeDirctoryToRoot(List<String> currentPath) {
        currentPath.clear();
    }

    public void changeToCurrentDirectory(List<String> currentPath) {
        for (int i = 0; i < currentPath.size(); i++) {
            root = (Directory) root.getDirectory(currentPath.get(i));
        }
    }

    public boolean checkIfYouAreInTheRightDirectory(List<String> currentPath) {
        return root.getName().equals(currentPath.get(currentPath.size() - 1));
    }

    public void goBackToRoot() {
        while (root.getParent() != null) {
            root = (Directory) root.getParent();
        }
    }

    public Directory getRoot() {
        return root;
    }

    public File getFile(String fileName) {
        return root.getFile(fileName);
    }
}
