package terminal;

import java.io.FileNotFoundException;
import java.nio.file.FileAlreadyExistsException;
import java.util.Arrays;
import java.util.List;
import java.util.MissingResourceException;

import com.sun.media.sound.InvalidFormatException;

import command.Command;
import consoleinputmethod.ConsoleInput;
import filesystem.FileSystem;
import parser.Parser;

public class Terminal {

    @SuppressWarnings("unused")
    private FileSystem fileSystem;
    private List<String> currentPath;
    private Parser parser;

    public Terminal(FileSystem fileSystem, List<String> currentPath) {
        this.fileSystem = fileSystem;
        this.currentPath = currentPath;
        parser = new Parser(fileSystem);
    }

    public void run() {
        boolean breaker = true;
        while (breaker) {
            System.out.print("/" + String.join("/", currentPath) + "$");
            List<String> commandLine = Arrays.asList(ConsoleInput.typeText().trim().split(" "));
            if (commandLine.get(0).equals("quit")) {
                breaker = false;
            } else {
                try {
                    Command command = parser.getOperation(commandLine.get(0), commandLine);
                    if (command != null) {
                        String result = command.execute(currentPath);
                    }
                } catch (IllegalArgumentException e) {
                    System.out.println(e.getMessage());
                } catch (FileAlreadyExistsException e) {
                    System.out.println(e.getMessage());
                } catch (FileNotFoundException e) {
                    System.out.println(e.getMessage());
                } catch (InvalidFormatException e) {
                    System.out.println(e.getMessage());
                }
            }
        }
    }
}
